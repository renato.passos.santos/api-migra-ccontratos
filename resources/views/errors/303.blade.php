@extends('errors::layout')

@section('title', __('Erro na URL'))
@section('code', '303')
@section('message', __($exception->getMessage() ?: 'Erro na URL'))
