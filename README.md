# API - Atualizar versão do Sistema Conta

Ferramenta para atualziação do Sistema Conta, versão x.x.x para x.x.x.

## Tecnologia Utilizada

Desenvolvido em PHP utilizando framework Laravel, versão 6.x.

## Licença

A licença dessa ferramenta é GPLv2. Pedimos que toda implementação seja disponibilizada para a comunidade.

## Objetivos

O objetivo principal desta API é disponibilizar toda a base de dados do sistema conta atual para migrar para a nova versão.

## Versões, Requisitos, Instalação e Configuração

### Versões

- Laravel 6.x;
- PHP 7.1.8+;

### Requisitos para instalação

- Git
- Composer

### Instalação

Para baixar o conteúdo via Git utilize o seguinte comando:

```
git clone https://xxxxxxxx
```

Para instalação entre no diretorio "xxxxx" e execute o comando composer:
```
cd xxxxx
composer install
```

### Configuração

Execute os seuintes comandos na raiz do seu projeto:
```
cp .env.example .env
php artisan key:generate
```
 > Esta chave gerada pelo comando `php artisan key:generate` deverá ser encaminhada à AGU para que tenha acesso aos dados do sistema conta local.

### Banco de Dados

Configure as informações de acesso ao banco de dados do seu sistema local no arquivo `.env`, localizado na raiz do seu projeto:
```
DB_CONNECTION=pgsql(mysql)
DB_HOST=nome_do_servidor_de_bando_de_dados
DB_PORT=5432(3306)
DB_DATABASE=conta
DB_USERNAME=seu_usuario
DB_PASSWORD=sua_senha
```

### Testes

Inicie o servidor PHP local para rodar a aplicação, na raiz do seu projeto execute o seguinte comando:

```
php artisan serve
```
> Caso já tenha algum serviço web ativo como o xampp ignore esta etapa anterior.


Após as configurações do banco e da chave de acesso, poderá testar a API acessando:

**http://localhost:8000/api/v1/contrato?token=sua_chave_gerada_pelo_comando_php_artisan_key:generate**

> Onde "contrato" é o nome da tabela que deseja buscar os dados.

## Configurações Especiais

Para publicar uma nova tabela, basta criar um Model na pasta *./app* seguindo os padrões do Laravel. 

Caso sejam diversas tabelas basta inserir o nome da tabela no `Array $tables` no arquivo ./routes/console.php e rodar o seguinte comando:

```
php artisan conta:models:all
```