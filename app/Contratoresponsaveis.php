<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contratoresponsaveis extends Model
{
    

    protected $table = 'contratoresponsaveis';
    protected $primaryKey = 'id';

    // ***** MUTATORS *****
    public function getInstalacaoIdAttribute($value){
        $retorno = $this->formatarAtributoInstalacaoId($value);
        return $retorno;
    }
    public function getFuncaoIdAttribute($value){
        $retorno = $this->formatarAtributoFuncaoId($value);
        return $retorno;
    }
    public function getUserIdAttribute($value){
        $usuario = $this->buscaUser($value);
        return $usuario;
    }

    // Métodos que auxiliam os mutators
    public function formatarAtributoInstalacaoId($id){
        $retorno = Instalacoes::find($id);
        if($retorno){return $retorno->nome;}
        else {return null;}
    }
    public function formatarAtributoFuncaoId($id){
        $retorno = Codigoitens::find($id);
        if($retorno){return $retorno->descricao;}
        else {return null;}
    }
    private function buscaUser($id){
        $retorno = Users::find($id);
        return $retorno->cpf . '|' . $retorno->name . '|' . strtolower($retorno->email);
    }
}
