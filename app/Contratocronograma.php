<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contratocronograma extends Model
{
    protected $table = 'contratocronograma';

    protected $primaryKey = 'id';

}
