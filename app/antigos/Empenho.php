<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empenho extends Model
{
    protected $table = 'empenho';

    protected $primaryKey = 'emp_id';
}
