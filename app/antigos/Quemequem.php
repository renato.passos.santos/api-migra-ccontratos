<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quemequem extends Model
{
    protected $table = 'quemequem';

    protected $primaryKey = 'qeq_id';


    public function getQeqFuncaoAttribute($value)
    {
        $descricao = $this->buscaFuncao($value);

        return $descricao;
    }

    public function getQeqUserCodAttribute($value)
    {
        $usuario = $this->buscaUser($value);

        return $usuario;
    }

    private function buscaFuncao($id)
    {
        $retorno = Codigo_item::find($id);

        return $retorno->cit_descricao;
    }

    private function buscaUser($id)
    {
        $retorno = Sec_users::find($id);

        return $retorno->login . '|' . $retorno->name . '|' . strtolower($retorno->email);
    }


    public function user()
    {
        return $this->belongsTo(Sec_users::class, 'qeq_user_cod');
    }

    public function funcao()
    {
        return $this->belongsTo(Codigo_item::class, 'qeq_funcao', 'cit_id');
    }

    public function contrato()
    {
        return $this->belongsTo(Contrato::class, 'qeq_con_id');
    }

}
