<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Justificativa extends Model
{
    protected $table = 'justificativa';

    protected $primaryKey = 'jus_id';
}
